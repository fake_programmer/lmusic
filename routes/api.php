<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\User\UserController;
use App\Http\Controllers\Auth\LoginController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**************************************All Api Route**************************************/
Route::group(['prefix' => 'v1'], function () {
    Route::post('login', [LoginController::class, 'login']);
    Route::middleware('auth:api')->group(function () {

        /****************************User Api Route****************************/
        Route::get('users/active-list', [UserController::class, 'active_list'])->name('users_active_list');
        Route::get('users/inactive-list', [UserController::class, 'inactive_list'])->name('users_inactive_list');
        Route::get('users/change-status/{id}', [UserController::class, 'change_status'])->name('user_change_status');
        Route::apiResource('users', UserController::class);
    });
});
