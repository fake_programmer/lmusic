<?php

namespace App\Http\Controllers;

use App\Libraries\WebApiResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Illuminate\Support\Arr;

class UserController extends Controller
{
    /**
     * Display List Users
     * @group  User Management
     * @return \Illuminate\Http\Response
     * @response 200
     * {"status":"success","message":"Show List Successfully","code":200,"data":[{"id":1,"name":"head account office","name_bn":"প্রধান হিসাব রক্ষণ কর্মকর্তার  কাৰ্যালয় ","status":1,"deleted_at":null,"created_at":"2021-03-04T11:06:57.000000Z","updated_at":"2021-03-04T11:06:57.000000Z"},{"id":2,"name":"divisional  account office","name_bn":"বিভাগীয় হিসাব রক্ষণ কর্মকর্তার  কাৰ্যালয় ","status":1,"deleted_at":null,"created_at":"2021-03-04T11:06:57.000000Z","updated_at":"2021-03-04T11:06:57.000000Z"},{"id":3,"name":"district  account office","name_bn":"জেলা হিসাব রক্ষণ কর্মকর্তার  কাৰ্যালয় ","status":1,"deleted_at":null,"created_at":"2021-03-04T11:06:57.000000Z","updated_at":"2021-03-04T11:06:57.000000Z"},{"id":4,"name":"upazila  account office","name_bn":"উপজেলা হিসাব রক্ষণ কর্মকর্তার  কাৰ্যালয় ","status":1,"deleted_at":null,"created_at":"2021-03-04T11:06:57.000000Z","updated_at":"2021-03-04T11:06:57.000000Z"}]}
     */

    public function index(Request $request)
    {
        $data = User::orderBy('id','DESC')->paginate(5);
        return view('users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name','name')->all();
        return view('users.create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);

        $user = User::create($input);
        $user->assignRole($request->input('roles'));

        return redirect()->route('users.index')
            ->with('success','User created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('users.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::pluck('name','name')->all();
        $userRole = $user->roles->pluck('name','name')->all();

        return view('users.edit',compact('user','roles','userRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all();
        if(!empty($input['password'])){
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = Arr::except($input,array('password'));
        }

        $user = User::find($id);
        $user->update($input);
        DB::table('model_has_roles')->where('model_id',$id)->delete();

        $user->assignRole($request->input('roles'));

        return redirect()->route('users.index')
            ->with('success','User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('users.index')
            ->with('success','User deleted successfully');
    }
}