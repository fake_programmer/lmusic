<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Libraries\WebApiResponse;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{

    /**
     * Display All Users
     * @group  User Management
     * @return \Illuminate\Http\Response
     * @response 200
     * {
        "status":"success",
        "message":"messages.success_show_all",
        "code":200,
        "data":[
                {
                    "id":2,
                    "name":"Sumon",
                    "email":"s@gmail.com",
                    "email_verified_at":null,
                    "created_at":"2021-05-25T07:28:20.000000Z",
                    "updated_at":"2021-05-25T07:28:20.000000Z"
                },
                {
                    "id":1,
                    "name":"Roktim Ariyan",
                    "email":"admin@gmail.com",
                    "email_verified_at":null,
                    "created_at":"2021-05-25T06:46:22.000000Z",
                    "updated_at":"2021-05-25T06:46:22.000000Z"
                }
            ]
        }
     */
    public function index(Request $request)
    {
        try {
            $data =  User::latest()->get()->toArray();
            return WebApiResponse::success(200, $data, 'All User');

        } catch (\Throwable $th) {
            return WebApiResponse::error(404, $errors = [], 'Something Went Wrong. Please Try Again');
        }

    }

    /**
     * Display Active Users List
     * @group  User Management
     * @return \Illuminate\Http\Response
     * @response 200
     * {
        "status": "success",
        "message": "All Active User",
        "code": 200,
        "data": [
                {
                    "id": 1,
                    "name": "Roktim Ariyan",
                    "email": "admin@gmail.com",
                    "email_verified_at": null,
                    "status": 1,
                    "created_at": "2021-05-26T10:57:37.000000Z",
                    "updated_at": "2021-05-26T10:57:37.000000Z",
                    "deleted_at": null
                }
            ]
        }
     */
    public function active_list()
    {
        try {
            $data =  User::whereStatus(1)->latest()->get()->toArray();
            return WebApiResponse::success(200, $data, 'All Active User');

        } catch (\Throwable $th) {
            return WebApiResponse::error(404, $errors = [], 'Something Went Wrong. Please Try Again');
        }

    }

    /**
     * Display Inactive Users List
     * @group  User Management
     * @return \Illuminate\Http\Response
     * @response 200
     * {
        "status": "success",
        "message": "All Inactive User",
        "code": 200,
        "data": [
                {
                    "id": 1,
                    "name": "Roktim Ariyan",
                    "email": "admin@gmail.com",
                    "email_verified_at": null,
                    "status": 0,
                    "created_at": "2021-05-26T10:57:37.000000Z",
                    "updated_at": "2021-05-26T10:57:37.000000Z",
                    "deleted_at": null
                }
            ]
        }
     */
    public function inactive_list()
    {
        try {
            $data =  User::whereStatus(0)->latest()->get()->toArray();
            return WebApiResponse::success(200, $data, 'All Inactive User');

        } catch (\Throwable $th) {
            return WebApiResponse::error(404, $errors = [], 'Something Went Wrong. Please Try Again');
        }

    }


    /**
     * Create New User
     * @group  User Management
     * @param  \Illuminate\Http\Request  $request
     * @bodyParam  name int required User Name. Example: Roktim
     * @bodyParam  email email required User Email. Example: roktim@gmail.com
     * @bodyParam  password string required Password. Example: 123456
     * @bodyParam  roles int required Role id. Example: 1
     * @return \Illuminate\Http\Response
     * @response 200
     * {
        "status": "success",
        "message": "User created successfully",
        "code": 201,
        "data": {
                "name": "Roktim",
                "email": "r@gmail.com",
                "updated_at": "2021-05-25T12:21:30.000000Z",
                "created_at": "2021-05-25T12:21:30.000000Z",
                "id": 4,
                "roles": [
                    {
                        "id": 1,
                        "name": "Admin",
                        "guard_name": "web",
                        "created_at": "2021-05-25T06:46:22.000000Z",
                        "updated_at": "2021-05-25T06:46:22.000000Z",
                    }
                ]
            }
        }
     */

    public function store(Request $request)
    {
        try {
            $custom_messages = [
                'name.required' => 'Select Office Type.',
                'email.required' => 'Select Office Lead Category.',
                'password.required' => 'The Office Name English field is required.',
                'roles.required' =>  'The Office Name Bangla field is required.',

            ];

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email|unique:users,email',
                'password' => 'required',
                'roles' => 'required'
            ],$custom_messages);

            if ($validator->fails()) {
                return WebApiResponse::validationError($validator, $request);
            }

            $data = [
                'name'      => $request->name,
                'email'      => $request->email,
                'password'      => Hash::make($request->password),
            ];

            $user = User::create($data);
            $user->assignRole($request->input('roles'));
            $token = $user->createToken('NRB')->accessToken;

            if ($user) {
                return WebApiResponse::success(201, $user->toArray(), trans('User created successfully'));
            }else {
                return WebApiResponse::error(400, $errors = [], trans('Something Went Wrong. Please Try Again.'));
            }

        } catch (\Throwable $th) {
            return WebApiResponse::error(404, $errors = [], 'Something Went Wrong. Please Try Again');
        }
    }

    /**
     * Display The Specified User.
     * @group  User Management
     * @param  \Illuminate\Http\Request  $request
     * @urlParam id required Id of user. Example: 1
     * @return \Illuminate\Http\Response
     * @response 200
     * {
        "status": "success",
        "message": "User find successfully",
        "code": 200,
        "data": {
            "id": 1,
            "name": "Roktim Ariyan",
            "email": "admin@gmail.com",
            "email_verified_at": null,
            "created_at": "2021-05-25T06:46:22.000000Z",
            "updated_at": "2021-05-25T06:46:22.000000Z"
            }
        }
     */
    public function show($id)
    {
        try {
            $findUser = User::find($id);
            if ($findUser){
                $user = $findUser->toArray();
                return WebApiResponse::success(200, $user, 'User find successfully');
            }else{
                return WebApiResponse::error(404, $errors = [], 'User not found');
            }
        } catch (\Throwable $th) {
            return WebApiResponse::error(404, $errors = [], 'Something Went Wrong. Please Try Again');
        }
    }

    /**
     * Update Existing User
     * @group  User Management
     * @param  \Illuminate\Http\Request  $request
     * @urlParam id required Id of user  . Example: 1
     * @bodyParam  name string required in user. Example: Roktim
     * @bodyParam  email email required User Email. Example: roktim@gmail.com
     * @bodyParam  password string required Password. Example: 123456
     * @bodyParam  roles int required Role id. Example: 1
     * @return \Illuminate\Http\Response
     * @response 201
     * {
        "status": "success",
        "message": "User Updated successfully",
        "code": 201,
        "data": {
            "id": 2,
            "name": "NRB",
            "email": "nrb@gmail.com",
            "email_verified_at": null,
            "created_at": "2021-05-26T08:31:43.000000Z",
            "updated_at": "2021-05-26T08:33:10.000000Z",
            "deleted_at": null,
            "roles": [
                {
                "id": 1,
                "name": "Admin",
                "guard_name": "web",
                "created_at": "2021-05-26T08:29:38.000000Z",
                "updated_at": "2021-05-26T08:29:38.000000Z",
                "pivot": {
                    "model_id": 2,
                    "role_id": 1,
                    "model_type": "App\\Models\\User"
                    }
                }
            ]
        }
    }
     */

    public function update(Request $request, $id)
    {

        try {

            $custom_messages = [
                'name.required' => 'User Name Required.',
                'email.required' => 'User Email Required.',
                'email.email' => 'User Email Must Be An Email.',
                'email.unique' => 'User Email Already Exists.',
                'password.required' => 'User Password Required.',
                'roles.required' =>  'User Role Required.',

            ];

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email|unique:users,email,'.$id,
                'password' => 'required',
                'roles' => 'required'
            ],$custom_messages);

            if ($validator->fails()) {
                return WebApiResponse::validationError($validator, $request);
            }

            $data = [
                'name'      => $request->name,
                'email'      => $request->email,
                'password'      => Hash::make($request->password),
            ];

            $findUser = User::find($id);
            if ($findUser){
                DB::table('model_has_roles')->where('model_id',$id)->delete();
                $findUser->assignRole($request->roles);
                $userUpdate = $findUser->update($data);
                $user = $findUser;

                return WebApiResponse::success(201, $user->toArray(), 'User Updated successfully');
            }else{
                return WebApiResponse::error(400, $errors = [], 'User not found. Please Try Again.');
            }

        } catch (\Throwable $th) {
            return WebApiResponse::error(404, $errors = [], 'Something Went Wrong. Please Try Again');
        }
    }

    /**
     * Remove Specific User
     * @group  User Management
     * @param  \Illuminate\Http\Request  $request
     * @urlParam id required Id of user. Example: 1
     * @return \Illuminate\Http\Response
     * @response 200 {"status":"success","message":"Individual User Deleted Successfully","code":200,"data":[]}
     */
    public function destroy($id)
    {
        try {
            $user = User::find($id);
            if ($user){
                $user->delete();
                $user = [];
                return WebApiResponse::success(200, $user, 'User Deleted Successfully');
            }else{
                return WebApiResponse::error(400, $errors = [], 'User Not Found.');
            }


        }catch(\Throwable $th) {
            return WebApiResponse::error(400, $errors = [], 'Something Went Wrong. Please Try Again');
        }

    }

    /**
     * Specific User Status Change
     * @group  User Management
     * @param  \Illuminate\Http\Request  $request
     * @urlParam id required Id of user. Example: 1
     * @return \Illuminate\Http\Response
     * @response 200 {
        "status": "success",
        "message": "User Active Successfully",
        "code": 200,
        "data": {
            "id": 2,
            "name": "NRB",
            "email": "1",
            "email_verified_at": null,
            "status": 1,
            "created_at": "2021-05-26T11:16:01.000000Z",
            "updated_at": "2021-05-26T12:18:20.000000Z",
            "deleted_at": null
            }
        }
     */
    public function change_status($id)
    {
        try {
            $findUser = User::find($id);
            if ($findUser){
                $findUser->update(['status'=> $findUser->status == 1 ? 0 : 1]);

                $user = $findUser->toArray();
                $message = $user['status'] == 1 ? 'User Active Successfully' : 'User Inactive Successfully';

                return WebApiResponse::success(200, $user, $message);
            }else{
                return WebApiResponse::error(400, $errors = [], 'User Not Found.');
            }


        }catch(\Throwable $th) {
            return WebApiResponse::error(400, $errors = [], 'Something Went Wrong. Please Try Again');
        }

    }
}