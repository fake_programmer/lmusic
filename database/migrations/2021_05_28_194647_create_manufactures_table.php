<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManufacturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manufactures', function (Blueprint $table) {
            $table->id();
            $table->string('name', 64);
            $table->string('phone', 32);
            $table->string('email', 32);
            $table->string('contact_person', 64);
            $table->string('contact_person_phone', 64);
            $table->string('city', 32);
            $table->decimal('total_pay_amount', 8, 2)->default('0.00');
            $table->decimal('total_due_amount', 8, 2)->default('0.00');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manufactures');
    }
}
