<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicines', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('qr_code');
            $table->string('name', 64);
            $table->foreignId('medicine_type_id')->constrained()->onDelete('cascade');
            $table->foreignId('category_id')->constrained()->onDelete('cascade');
            $table->foreignId('unit_id')->constrained()->onDelete('cascade');
            $table->foreignId('generic_id')->constrained()->onDelete('cascade');
            $table->foreignId('leaf_id')->constrained()->onDelete('cascade');
            $table->foreignId('shelf_id')->constrained()->onDelete('cascade');
            $table->foreignId('manufacture_id')->constrained()->onDelete('cascade');
            $table->text('details');
            $table->decimal('manufacture_price', 8, 2);
            $table->decimal('price', 8, 2);
            $table->string('image', 64);
            $table->integer('status')->comment('1= active & 0= inactive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicines');
    }
}
